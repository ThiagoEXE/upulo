# UPulo
A ausência em meio digital das pequenas empresas, traz na crise do Covid-19 um grande problema: acessar os clientes; 
com isso, nós dá UPulo trouxemos pelo WhatsApp, Telegram e Messenger um mecanismo capaz de aglomerar e ordenar empresas 
por bairro e segmento e  trazer os clientes para as empresas sem tirá-los de casa.

# Como Acessar?
Para acessar nossa solução você deve baixar o arquivo LandingPage.html, após baixa-lo, clique no arquivo para abrir depois 
clique no link **Cadastre-se Aqui** e você será direcionado para um formulário, nele você pode preencher com as informações 
referentes ao seu negócio para que ele seja inserido por nossa equipe no chatbot. 

Retorne com o botão de voltar do navegador para a Landing page e agora clique no link **UPulo Whatsapp** após clicar você será 
direcionado para o chatbot do whatsapp, é preciso está com o whatsapp web ou aplicativo do whatsapp conectado sendo assim você 
poderá enviar um “oi” e interagir com as opções e informar se quer contratar um serviço ou um produto.

Você pode também na “Landing Page”, acessar o chatbot do Telegram clicando no link **UPulo Telegram**, a partir daí pode enviar um “oi” 
e começar a interagir com o chatbot, é preciso está com uma conta conectada no Telegram para que haja a interação.

Teste também a interação com o Messenger, volte na Landing Page e clique no link **UPulo Messenger** você será enviado para o messenger, porém 
para interagir será preciso fazer uma solicitação de amizade e aguardar a solicitação ser aceita, após aceito o pedido você pode mandar um “oi” para começar a interação.



# Exemplo de conversa pelo ChatBot:

**Cliente:** 
Oi

**ChatBot Upulo:**
 Olá, tudo bem? Sou assistente virtual da Upulo e gostaria de divulgar o nosso trabalho para você. Somos uma plataforma online que integramos o cliente com o vendedor.

Só pra ficar claro, não somos um Ifood ou algo do tipo...

Nossa missão é ajudar os micro e pequenos empreendedores nessa crise, levando até você produtos/serviços que antes eram impossíveis de se conseguir no meio dessa pandemia.

Digite Continuar para ir ao Menu.

**Cliente:**
Continuar

**ChatBot Upulo:**
Vamos lá!
Começaremos nossa busca por produtos e serviços perto de você.
Se está interessado em produtos, digite Produtos.
Se está interessado em serviços, digite Serviços.

**Cliente:**
Produtos


**ChatBot Upulo:**
Vamos lá!
Agora escolha o segmento de produtos que deseja procurar:
Digite 1 para -> Restaurantes
Digite 2 para -> Mercados, padarias e delicatessen
Digite 3 para -> Moda
Digite 4 para -> Material de construção
Digite 5 para -> Farmácia
Digite 6 para -> Outros

Caso deseje mudar para opção serviços, digite: Serviços

**Cliente:**
3

**ChatBot Upulo:**
Opção Moda

Para achar lojas relacionadas a moda perto de você digite o “3” e o nome do seu bairro (Não é necessário colocar acento).

EXEMPLO:
3itaigara
3sussuarana
3saomarcos

Caso queira voltar ao menu anterior digite Produtos

⚠️Se após colocar o seu bairro não houver resposta, o motivo é: Ainda não há cadastro de nenhum segmento em seu bairro.⚠️

**Cliente:**
3cajazeiras

**ChatBot Upulo:**
Moda em cajazeiras
Essa é a lista com as lojas de Moda conectadas com a gente até o momento:
Empresa: Bia Modas (Rua Juscelino Kubitschek n 54 cajazeira 11)
Link para contato: https://api.whatsapp.com/send?phone=5571986073350
 
Caso tenha escolhido o bairro errado, digite 3

 

